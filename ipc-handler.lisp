(in-package :spectrum2-ipc)

(defclass spectrum2-ipc-handler ()
  ((conn
    :accessor conn
    :initarg :conn)
   (signaled-read-condition
    :accessor signaled-read-condition
    :initform nil)
   (lock
    :reader lock
    :initform (sb-thread:make-mutex :name "spectrum2 ipc handler lock"))
   (last-ping-sent
    :accessor last-ping-sent
    :initform 0)
   (ping-outstanding
    :accessor ping-outstanding
    :initform nil)))

(defconstant +ipc-ping-interval+ 10)
(defconstant +ipc-read-deadline+ 5)

(defvar *ipc-in-handler-context* nil
  "When inside a HANDLE-IPC-MESSAGE call, this is locally bound to T.")

(defun ipc-handler-write-inner (handler message)
  "Implementation of IPC-HANDLER-WRITE that assumes HANDLER is locked."
  (when (signaled-read-condition handler)
    (signal (signaled-read-condition handler)))
  (write-ipc-message (conn handler) message))

(defun ipc-handler-write (handler message)
  "Writes the IPC message object MESSAGE to HANDLER's connection, locking the HANDLER if called from outside a HANDLE-IPC-MESSAGE call. If a read error caused the HANDLER's read thread to die, no write operation is performed and that condition is signaled."
  (if *ipc-in-handler-context*
      (ipc-handler-write-inner handler message)
      (sb-thread:with-mutex ((lock handler))
        (ipc-handler-write-inner handler message))))

(defun maybe-send-ping (handler)
  "If the HANDLER needs to send a ping, sends it, unless a ping is already outstanding, in which case an error is signaled."
  (check-type handler spectrum2-ipc-handler)
  (when (>= (- (get-universal-time) (last-ping-sent handler))
            +ipc-ping-interval+)
    (if (ping-outstanding handler)
        (error "Spectrum2 IPC handler didn't respond to the last ping!")
        (progn
          (sb-thread:with-mutex ((lock handler))
            (write-ipc-message (conn handler) (make-instance 'ipc-ping))
            (setf (last-ping-sent handler) (get-universal-time))
            (setf (ping-outstanding handler) t))))))

#-sbcl (error "Lisps other than SBCL are currently unsupported, sorry :(")

(defun maybe-read-ipc-message-with-pings (handler)
  "Reads a message from HANDLER's connection, or returns NIL if no message was read within +IPC-READ-DEADLINE+ seconds."
  (check-type handler spectrum2-ipc-handler)
  (prog1
      (handler-case
          (sb-sys:with-deadline (:seconds +ipc-read-deadline+)
            (read-ipc-message (conn handler)))
        (sb-sys:deadline-timeout () nil))
    (maybe-send-ping handler)))

(defgeneric handle-ipc-message (handler message)
  (:documentation "Handle the provided Spectrum2 IPC message MESSAGE. Methods can assume the HANDLER is locked for the duration of the function call, and that *IPC-IN-HANDLER-CONTEXT* is bound to T (an invariant which callers MUST uphold)."))

(defmethod handle-ipc-message ((handler spectrum2-ipc-handler) message)
  (warn "Unhandled IPC message: ~A" message))

(defmethod handle-ipc-message ((handler spectrum2-ipc-handler) (e error))
  (warn "Error in IPC read thread: ~A" e))

(defmethod handle-ipc-message ((handler spectrum2-ipc-handler) (message ipc-stats))
  ;; spectrum2 likes to send us stats on memory usage every $interval.
  ;; this can lead to a lot of "unhandled..." logspam.
  (declare (ignore handler message)))

(defmethod handle-ipc-message ((handler spectrum2-ipc-handler) (message ipc-pong))
  (declare (ignore message))
  (setf (ping-outstanding handler) nil))

(defmethod handle-ipc-message ((handler spectrum2-ipc-handler) (message ipc-ping))
  (declare (ignore message))
  (ipc-handler-write handler (make-instance 'ipc-pong)))

(defun ipc-message-reader-loop (handler)
  "Continually read messages from HANDLER's connection, and invoke HANDLE-IPC-MESSAGE on the received messages."
  (loop
    (handler-case
        (alexandria:when-let ((msg (maybe-read-ipc-message-with-pings handler))
                              (*ipc-in-handler-context* t))
          (sb-thread:with-mutex ((lock handler))
            (handle-ipc-message handler msg)))
      (error (e)
        (sb-thread:with-mutex ((lock handler))
          (setf (signaled-read-condition handler) e)
          (handle-ipc-message handler e)
          (return-from ipc-message-reader-loop))))))

(defun start-ipc-handler (handler)
  "Kick off the reader thread for the IPC handler HANDLER. Returns the read thread object."
  (check-type handler spectrum2-ipc-handler)
  (sb-thread:make-thread (lambda ()
                           (ipc-message-reader-loop handler))
                         :name "spectrum2 ipc handler thread"))

;; Useful sexps to test the above code in the REPL:
#+(or) (progn
         (defparameter *socket* (usocket:socket-listen #(127 0 0 1) 7777 :reuse-address t))
         (defparameter *ipch* (make-instance 'spectrum2-ipc-handler :conn (usocket:socket-stream (usocket:socket-accept *socket* :element-type '(unsigned-byte 8)))))
         (start-ipc-handler *ipch*))
