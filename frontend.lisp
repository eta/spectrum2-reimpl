;;;; Spectrum 3: a reimplementation of (parts of) Spectrum 2, in Common Lisp

(defpackage :spectrum3
  (:use :cl))

(in-package :spectrum3)

(defvar *component* nil
  "The global singleton SPECTRUM3-COMPONENT object.")
(defvar *outgoing-messages-stack* nil)

(defun s3-ipc-handler-write (message)
  "Sends MESSAGE to the currently active IPC handler. This function MUST be called within a WITH-COMPONENT-CONTEXT macro invocation; if it's called outside, it signals an error."
  (unless *outgoing-messages-stack*
    (error "S3-IPC-HANDLER-WRITE called outside macro invocation"))
  (when (eql *outgoing-messages-stack* :empty)
    (setf *outgoing-messages-stack* nil))
  (push message *outgoing-messages-stack*))

(defmacro with-component-context ((component) &body body)
  "Grabs COMPONENT's data lock, sets up the necessary context for S3-HANDLER-IPC-WRITE to work, and then runs BODY. If any calls to S3-HANDLER-IPC-WRITE were made, sends the messages after dropping COMPONENT's data lock."
  `(let ((*outgoing-messages-stack* :empty)
         (ipch))
     (unwind-protect
          (sxcl::with-component-data-lock (,component)
            (setf ipch (ipc-handler ,component))
            ,@body)
       (unless (eql *outgoing-messages-stack* :empty)
         (when ipch ; NOTE: message dropping is possible!
           (loop
             for msg in *outgoing-messages-stack*
             do (s2i::ipc-handler-write ipch msg)))))))

(defmacro with-ipc-handler-context ((component handler) &body body)
  "Takes COMPONENT's data lock, checks that the IPC HANDLER is still associated with COMPONENT, and, if it is, executes BODY."
  `(sxcl::with-component-data-lock (,component)
     (if (eql (ipc-handler ,component) ,handler)
         (progn ,@body)
         (warn "Out-of-date IPC handler used; ignoring"))))

(defmacro define-ipc-message-handler ((msgtype handler payloadvar) &body body)
  "Shorthand for defining a new method for S2I::HANDLE-IPC-MESSAGE that calls WITH-IPC-HANDLER-CONTEXT before executing BODY."
  (alexandria:with-gensyms (localvar)
    `(defmethod s2i::handle-ipc-message ((,handler spectrum3-ipc-handler) (,localvar ,msgtype))
       (with-ipc-handler-context (*component* ,handler)
         (let ((,payloadvar (s2i::payload ,localvar)))
           ,@body)))))

(deftype ipc-state () '(member :disconnected :connecting :connected))

(defclass spectrum3-ipc-handler (s2i::spectrum2-ipc-handler) ()
  (:documentation "A Spectrum2 IPC handler. One is generated for every incoming backend connection."))

(define-ipc-message-handler (s2i::ipc-connected handler msg)
  (let ((user (pb:string-value (pbnetwork:user msg))))
    (format *debug-io* "~&connected: ~A~%" user)
    (setf (gethash user (user-states *component*)) :connected)
    (admin-msg *component* user "Connected to external chat network.")))

(define-ipc-message-handler (s2i::ipc-disconnected handler msg)
  (let ((user (pb:string-value (pbnetwork:user msg)))
        (error-message (pb:string-value (pbnetwork:message msg)))
        (error-code (pbnetwork:error msg)))
    (format *debug-io* "~&disconnected: ~A -- \"~A\" (~A)~%" user error-message error-code)
    (setf (gethash user (user-states *component*)) :disconnected)
    (admin-msg *component* user (format nil
                                        "Disconnected: \"~A\" (~A)"
                                        error-message error-code))))

(define-ipc-message-handler (s2i::ipc-room-list handler msg)
  (let* ((user (pb:string-value (pbnetwork:user msg)))
         (uid (get-user-id user))
         (rooms (map 'vector #'pb:string-value (pbnetwork:room msg)))
         (names (map 'vector #'pb:string-value (pbnetwork:name msg))))
    (format *debug-io* "~&roomlist for ~A (length: ~A)~%" user (length rooms))
    (loop
      for room across rooms
      for name across names
      do (progn
           (store-roomlist-name uid room name)
           ;; Just freaking join all the rooms in the list. While unorthodox,
           ;; this does mean we can figure out ahead of time which ones are
           ;; legit, and start receiving messages from them (for history / MAM).
           ;;
           ;; FIXME: We currently assume the other side is going to completely
           ;; discard the nickname field. This should be audited.
           (s2i::ipc-handler-write handler (s2i::make-join-room user "bridge" room))))))

(define-ipc-message-handler (s2i::ipc-join-room handler msg)
  (let* ((user (pb:string-value (pbnetwork:user-name msg)))
         (uid (get-user-id user))
         (room-name (pb:string-value (pbnetwork:room msg)))
         ;; This function creates the room if it doesn't exist.
         (room-id (intent-get-user-room-id uid room-name)))
    (format *debug-io* "~&joined '~A' (#~A) for ~A (#~A)~%" room-name room-id user uid)
    ;; Clear out the room participants, since they're about to get filled in.
    (clear-room-participants room-id)))

(define-ipc-message-handler (s2i::ipc-room-subject-changed handler msg)
  (let* ((user (pb:string-value (pbnetwork:user-name msg)))
         (uid (get-user-id user))
         (room-name (pb:string-value (pbnetwork:buddy-name msg)))
         (room-id (intent-get-user-room-id uid room-name))
         (subject (pb:string-value (pbnetwork:message msg)))
         (setter (nil-empty (pb:string-value (pbnetwork:nickname msg)))))
    (format *debug-io* "~&in '~A' (#~A) for ~A (#~A): '~A' set subject '~A'" room-name room-id user uid setter subject)
    (set-room-subject room-id subject setter)))

(define-ipc-message-handler (s2i::ipc-room-information handler msg)
  (let* ((user (pb:string-value (pbnetwork:user msg)))
         (uid (get-user-id user))
         (room-name (pb:string-value (pbnetwork:room msg)))
         (room-id (intent-get-user-room-id uid room-name))
         (name (pb:string-value (pbnetwork:name msg))))
    (format *debug-io* "~&in '~A' (#~A) for ~A (#~A): room info name '~A'" room-name room-id user uid name)
    (update-room-name-if-lower-than room-id name 1))) ; FIXME: magic

(define-ipc-message-handler (s2i::ipc-participant-changed handler msg)
  (let* ((user (pb:string-value (pbnetwork:user-name msg)))
         (uid (get-user-id user))
         (room-name (pb:string-value (pbnetwork:room msg)))
         ;; This'll break with stock spectrum2.
         (room-id (intent-get-user-room-id uid room-name))
         (nickname (pb:string-value (pbnetwork:nickname msg)))
         (moderatorp (eql (pbnetwork:flag msg) 1)) ; Magic number is magic.
         (newname (nil-empty (pb:string-value (pbnetwork:newname msg)))))
    (format *debug-io* "~&in '~A' (#~A) for ~A (#~A): " room-name room-id user uid)
    (if newname
        (progn
          (format *debug-io* "'~A' -> '~A'~%" nickname newname)
          (update-room-participant room-id nickname newname moderatorp))
        (progn
          (format *debug-io* "'~A' added (moderator: ~A)~%" nickname moderatorp)
          (insert-room-participant room-id nickname moderatorp)))))

;; This code is incredibly naive and will undoubtedly need an overhaul...
(define-ipc-message-handler (s2i::ipc-conv-message handler msg)
  (let* ((user (pb:string-value (pbnetwork:user-name msg)))
         (uid (get-user-id user))
         (buddy (pb:string-value (pbnetwork:buddy-name msg)))
         (from-jid (concatenate 'string buddy "@" (sxcl::component-name *component*)))
         (room-id (get-user-room-id uid buddy))
         (message (pb:string-value (pbnetwork:message msg))))
    (format *debug-io* "~&~A~%" msg)
    (format *debug-io* "~&conv-message for ~A (#~A) from ~A (room id #~A)~%" user uid buddy room-id)
    (unless room-id ; rooms hard
      (sxcl::with-message (*component* user
                           :from from-jid)
        (cxml:with-element "body"
          (cxml:text message))))))

(defmethod s2i::handle-ipc-message ((handler spectrum3-ipc-handler) (e error))
  (with-ipc-handler-context (*component* handler)
    (format *debug-io* "~&!! IPC handler failed: ~A~%" e)
    (handle-ipc-handler-disconnect *component*)
    (setf (ipc-handler *component*) nil)))

(defclass spectrum3-component (sxcl::xmpp-component)
  ((ipc-handler
    :accessor ipc-handler
    :initform nil
    :documentation "The currently-in-use SPECTRUM3-IPC-HANDLER.")
   (backend-listener-thread
    :accessor backend-listener-thread
    :initform nil
    :documentation "A thread handle for the backend listener thread, responsible for establishing new Spectrum2 IPC connections.")
   (user-states
    :accessor user-states
    :initform (make-hash-table :test 'equal)
    :documentation "A hash table mapping user jid => IPC-STATE, describing the current backend state of the users' connections.")))

(defun handle-ipc-handler-disconnect (comp)
  "Cleans up after an IPC handler disconnects on COMP. (The lock must be taken out.)"
  (loop
    for jid being the hash-keys of (user-states comp)
      using (hash-value state)
    do (unless (eql state :disconnected)
         (admin-msg comp jid "Disconnected due to backend problems; will reconnect when possible.")
         (setf (gethash jid (user-states comp)) :disconnected))))

(defun backend-listener-thread-loop (comp host port)
  "Starts a TCP server on HOST:PORT, creating new instances of SPECTRUM3-IPC-HANDLER and changing COMP to use the latest one each time one comes in."
  (let ((socket (usocket:socket-listen host port
                                       :element-type '(unsigned-byte 8)
                                       :reuse-address t)))
    (format *debug-io* "~&-> Listening on ~A port ~A for new spectrum2 backends...~%" host port)
    (unwind-protect
         (loop
           (alexandria:when-let*
               ((client-sock (handler-case
                                 (usocket:socket-accept socket)
                               (error (e)
                                 (warn "Error calling SOCKET-ACCEPT: ~A" e))))
                (new-handler (make-instance 'spectrum3-ipc-handler
                                            :conn (usocket:socket-stream client-sock))))
             (sxcl::with-component-data-lock (comp)
               (format *debug-io* "~&-> New spectrum2 backend connection~%")
               (alexandria:when-let ((old-handler (ipc-handler comp)))
                 (format *debug-io* "~&   Cleaning up after old connection...~%")
                 (handle-ipc-handler-disconnect comp))
               (setf (ipc-handler comp) new-handler)
               (format *debug-io* "~&   Starting new IPC handler...~%")
               (s2i::start-ipc-handler new-handler))))
      (ignore-errors
       (usocket:socket-close socket)))))

(defun admin-jid (comp)
  "Get the admin JID for COMP. You need the lock to be taken out for this one."
  (concatenate 'string "admin@" (sxcl::component-name comp) "/adminbot"))

(defun admin-msg (comp jid text)
  "Send an admin message from the admin on COMP to JID."
  (sxcl::send-text-message comp jid text (admin-jid comp)))

(defparameter *admin-help-text*
  "** Spectrum 3, a theta.eu.org project **

- register USERNAME [PASSWORD]: register authentication credentials and log in
- connect: use stored credentials to connect
- stop: disconnect, and stop attempting to connect in future
- help: view this help text")

(defun send-ipc-login (jid)
  "Retrieves credentials for the given JID, and sends an IPC login command for that user. MUST be called within component context."
  (multiple-value-bind (username password)
      (get-user-connection-credentials jid)
    (format *debug-io* "~&-> Logging in user ~A~%" jid)
    (s3-ipc-handler-write (s2i::make-ipc-login jid username password))))

(defun handle-admin-command (comp from obody uid)
  "Handler for commands sent to admin@, containing BODY, from the user with barejid FROM and user ID UID (if non-NIL)"
  (labels ((reply (text)
             (admin-msg comp from text)))
    (let ((body (string-downcase obody)))
      (cond
        ((equal body "help")
         (reply *admin-help-text*))
        ((uiop:string-prefix-p "register " body)
         (let ((args (split-sequence:split-sequence #\Space obody)))
           (if (>= 3 (length args) 2)
               (progn
                 (register-user from (elt args 1)
                                (or
                                 (ignore-errors (elt args 2))
                                 ""))
                 (reply "Credentials registered; connecting...")
                 (send-ipc-login from))
               (reply "Syntax: `register USERNAME [PASSWORD]`"))))
        ((and (equal body "connect")
              (get-user-connection-credentials from))
         (progn
           (reply "Connecting...")
           (send-ipc-login from)))
        ((equal body "connect")
         (reply "You don't have stored authentication credentials; try the `register` command instead."))
        (t
         (reply "Unknown command. Try `help` for a list of supported commands,."))))))

(defun handle-muc-join (comp from muc-localpart muc-id nickname)
  (format *debug-io* "~A joining MUC ~A with roomnick ~A~%" from muc-localpart nickname)
  (let* ((muc-jid (concatenate 'string muc-localpart "@" (sxcl::component-name comp)))
         (old-nickname (get-room-user-nickname muc-id))
         (nickname-to-use (or old-nickname nickname))
         (subject (get-room-subject muc-id)))
    (labels
        ((send-presence (user-nickname affiliation role &key stati user-jid)
           (sxcl::send-muc-presence comp from muc-jid user-nickname affiliation role :stati stati :user-jid user-jid)))
      ;; send member list
      (loop
        for (nickname . moderatorp) in (get-room-participants muc-id)
        do (unless (string= nickname nickname-to-use) ; XXX: oopsie woopsie!
             (send-presence nickname
                            (if moderatorp "admin" "member")
                            (if moderatorp "moderator" "participant"))))
      ;; send self-presence when the member list is done
      (send-presence nickname-to-use "member" "participant"
                     :stati (append '("110")
                                    (unless (string= nickname old-nickname)
                                      ;; 210 = "we forced your roomnick"
                                      '("210"))))
      ;; send subject
      (sxcl::with-message (comp from
                           :from muc-jid
                           :type "groupchat")
        (cxml:with-element "subject"
          (cxml:text subject)))
      (unless old-nickname
        (set-room-user-nickname muc-id nickname-to-use))
      (insert-room-joined muc-id from))))

(defun disco-info-handler (comp &key to from &allow-other-keys)
  "Handles XEP-0030 disco#info requests."
  (format *debug-io* "~&disco#info: ~A~%" to)
  (with-component-context (comp)
    `((cxml:with-element "query"
        (cxml:attribute "xmlns" ,sxcl::+disco-info-ns+)
        ,@(let* ((uid (get-user-id from))
                 (room-id (get-user-room-id uid (sxcl::localpart to)))
                 (chat-subject (get-room-name room-id)))
            (cond
              ((equal to (sxcl::component-name comp))
               `((sxcl::disco-identity "spectrum3 bridge" "xmpp" "gateway")
                 (sxcl::disco-feature ,sxcl::+disco-info-ns+)
                 (sxcl::disco-feature ,sxcl::+disco-items-ns+)
                 (sxcl::disco-feature ,sxcl::+muc-ns+)))
              (chat-subject
               `((sxcl::disco-identity ,chat-subject "text" "conference")
                 (sxcl::disco-feature ,sxcl::+disco-info-ns+)
                 (sxcl::disco-feature ,sxcl::+muc-ns+)
                 ;;(sxcl::disco-feature ,+mam-ns+)
                 ;;(sxcl::disco-feature ,+muc-stable-id-ns+)
                 (sxcl::disco-feature ,sxcl::+unique-stanzas-ns+)
                 (sxcl::disco-feature "muc_persistent")))
              (t nil)))))))

(defun whatsxmpp-presence-unavailable-handler (comp &key from to &allow-other-keys)
  "Handles a presence unavailable broadcast."
  (with-component-data-lock (comp)
    (multiple-value-bind (to-hostname to-localpart)
        (parse-jid to)
      (declare (ignore to-hostname))
      (let* ((stripped (strip-resource from))
             (uid (get-user-id stripped))
             (chat-id (get-user-chat-id uid to-localpart)))
        (when (and uid (uiop:string-prefix-p "g" to-localpart))
          (format *debug-io* "~&~A muc-presence-unavailable: ~A~%" from to)
          (when chat-id
            (with-prepared-statements
                ((remove-joined-stmt "DELETE FROM user_chat_joined WHERE chat_id = ? AND jid = ?"))

              (bind-parameters remove-joined-stmt chat-id from)
              (sqlite:step-statement remove-joined-stmt))))))))

(defun spectrum3-presence-unavailable-handler (comp &key from stripped to &allow-other-keys)
  (with-component-context (comp)
    (let* ((uid (get-user-id stripped))
           (chat-id (get-user-room-id uid (sxcl::localpart to))))
      (when chat-id
        (format *debug-io* "~&~A muc-presence-unavailable: ~A~%" from to)
        (remove-room-joined chat-id from)))))

(defun spectrum3-presence-handler (comp &key to from stripped stanza &allow-other-keys)
  (with-component-context (comp)
    (let* ((uid (get-user-id stripped))
           (x-element (sxcl::get-node-with-xmlns (sxcl::child-elements stanza) sxcl::+muc-ns+))
           (muc-localpart (sxcl::localpart to))
           (nickname (sxcl::resource to))
           (chat-id (when uid
                      (get-user-room-id uid muc-localpart))))
      (when x-element
        (format *debug-io* "~&~A muc-presence: ~A~%" from to)
        (unless uid
          (signal 'sxcl::stanza-error
                  :defined-condition "registration-required"
                  :text "You must register to join MUCs via this bridge."
                  :type "auth"))
        (unless chat-id
          (signal 'sxcl::stanza-error
                  :defined-condition "item-not-found"
                  :text "Couldn't find a legacy network chatroom with that JID."
                  :type "modify"))
        (unless nickname
          (signal 'sxcl::stanza-error
                  :defined-condition "jid-malformed"
                  :text "Please specify a room nickname."
                  :type "modify"))
        (handle-muc-join comp from muc-localpart chat-id nickname)))))

(defun spectrum3-message-handler (comp &key from to stripped body id oob-url &allow-other-keys)
  (with-component-context (comp)
    (let* ((uid (get-user-id from))
           (to-localpart (sxcl::localpart to))
           (state (gethash stripped (user-states comp)))
           (target-user (when (uiop:string-prefix-p "b-" to-localpart)
                          (nil-empty (subseq to-localpart 2)))))
      (format *debug-io* "~&message ~A from ~A to ~A~%" id from to-localpart)
      (cond
        ((equal to-localpart "admin")
         (handle-admin-command comp stripped body uid))
        ((not uid)
         (signal 'sxcl::stanza-error
                 :defined-condition "registration-required"
                 :text "You must register with the transport admin (spectrum3@) to use this transport."
                 :type "auth"))
        ((not (eql state :connected))
         (signal 'sxcl::stanza-error
                 :defined-condition "recipient-unavailable"
                 :text "You're not currently connected to the external chat network. Check messages from the admin for further details."
                 :type "wait"))
        (target-user
         (s3-ipc-handler-write
          (s2i::make-conv-message stripped target-user body :id id)))
        (t
         (signal 'sxcl::stanza-error
                 :defined-condition "item-not-found"
                 :text "That target user isn't valid. External chat network users will have an address starting with b-, while groups will start with g-."
                 :type "modify"))))))

(defun spectrum3-init ()
  "Initialize the spectrum3 XMPP component."
  (tq::connect-database)
  (format *debug-io* "~&-> Starting Spectrum 3~%")
  (format *debug-io* "~&   Loading configuration~%")
  (tq::with-prepared-statement
      (config "SELECT server, port, listen_host, listen_port, component_name, shared_secret FROM configuration WHERE rev = 1")
    (assert (sqlite:step-statement config) () "No configuration in database!")
    (destructuring-bind (server port listen-host listen-port component-name shared-secret)
        (tq::column-values config)
      (when *component*
        (sxcl::with-component-data-lock (*component*)
          (format *debug-io* "~&   Destroying previous listener thread~%")
          (ignore-errors
           (sb-thread:terminate-thread (backend-listener-thread *component*)))))
      (let* ((comp (sxcl::make-component server port shared-secret component-name))
             (ret (change-class comp 'spectrum3-component)))
        (setf (backend-listener-thread comp)
              (sb-thread:make-thread
               (lambda ()
                 (backend-listener-thread-loop ret listen-host listen-port))
               :name "spectrum3 backend listener thread"))
        (event-emitter:on :text-message ret (lambda (&rest args)
                                              (apply #'spectrum3-message-handler ret args)))
        (event-emitter:on :presence ret (lambda (&rest args)
                                          (apply #'spectrum3-presence-handler ret args)))
        (event-emitter:on :presence-unavailable ret (lambda (&rest args)
                                                      (apply #'spectrum3-presence-unavailable-handler ret args)))
        (sxcl::register-component-iq-handler ret :disco-info #'disco-info-handler)
        (sxcl::register-component-iq-handler ret :ping (lambda (&rest args)
                                                         (declare (ignore args))
                                                         nil))
        (setf *component* ret)))))
