(in-package :spectrum3)

(defun nil-empty (seq)
  "If SEQ (a sequence) is empty, returns NIL; otherwise, returns SEQ."
  (unless (eql (length seq) 0) seq))
