(in-package :spectrum3)

(defun get-user-id (jid)
  "Get the user ID of JID (after stripping its resource), or NIL if none exists."
  (tq::with-prepared-statement
      (get-user "SELECT id FROM users WHERE jid = ?")
    (let ((stripped (sxcl::strip-resource jid)))
      (tq::bind-parameters get-user stripped)
      (when (sqlite:step-statement get-user)
        (first (tq::column-values get-user))))))

(defun get-user-jid (uid)
  "Get the user JID of UID."
  (tq::with-prepared-statement
      (get-user "SELECT jid FROM users WHERE id = ?")
    (tq::bind-parameters get-user uid)
    (when (sqlite:step-statement get-user)
      (first (tq::column-values get-user)))))

(defun get-component-name ()
  "Get the configured component name."
  (tq::with-prepared-statement
      (get-name "SELECT component_name FROM configuration WHERE rev = 1")
    (assert (sqlite:step-statement get-name))
    (first (tq::column-values get-name))))

(defun get-user-connection-credentials (jid)
  "Get the credentials used to connect to the legacy network for JID, returning (VALUES USERNAME PASSWORD) or NIL if none could be found."
  (tq::with-prepared-statement
      (get-creds "SELECT username, password FROM users WHERE jid = ?")
    (let ((stripped (sxcl::strip-resource jid)))
      (tq::bind-parameters get-creds stripped)
      (when (sqlite:step-statement get-creds)
        (values-list (tq::column-values get-creds))))))

(defun register-user (jid username password)
  "Register the user JID with the transport, using the USERNAME and PASSWORD to connect to the legacy network."
  (tq::with-prepared-statement
      (insert-user "INSERT INTO users (jid, username, password, broken) VALUES (?, ?, ?, false) ON CONFLICT (jid) DO UPDATE SET username = excluded.username, password = excluded.password, broken = excluded.broken")
    (let ((stripped (sxcl::strip-resource jid)))
      (tq::bind-parameters insert-user stripped username password)
      (sqlite:step-statement insert-user))))

(defun store-roomlist-name (for-uid jid name)
  "Register the room with localpart JID as having the specified NAME."
  (tq::with-prepared-statement
      (insert-rn "INSERT INTO user_room_names (user_id, jid, title) VALUES (?, ?, ?) ON CONFLICT (user_id, jid, title) DO UPDATE SET title = excluded.title")
    (tq::bind-parameters insert-rn for-uid jid name)
    (sqlite:step-statement insert-rn)))

(defun get-roomlist-name (for-uid jid)
  "Get a potential stored room name for the room with localpart JID."
  (tq::with-prepared-statement
      (get-rn "SELECT title FROM user_room_names WHERE user_id = ? AND jid = ?")
    (tq::bind-parameters get-rn for-uid jid)
    (when (sqlite:step-statement get-rn)
      (first (tq::column-values get-rn)))))

(defun get-room-name (room-id)
  "Get ROOM-ID's name. Returns the source as a second value."
  (tq::with-prepared-statement
      (get-rn "SELECT title, title_source FROM user_rooms WHERE id = ?")
    (tq::bind-parameters get-rn room-id)
    (when (sqlite:step-statement get-rn)
      (values-list (tq::column-values get-rn)))))

(defun get-room-subject (room-id)
  "Get ROOM-ID's subject."
  (tq::with-prepared-statement
      (get-rn "SELECT subject FROM user_room_subject WHERE room_id = ?")
    (tq::bind-parameters get-rn room-id)
    (when (sqlite:step-statement get-rn)
      (values-list (tq::column-values get-rn)))))

(defun get-room-user-nickname (room-id)
  "Get the user's joined nickname for ROOM-ID, or NIL if it is blank."
  (tq::with-prepared-statement
      (get-rs "SELECT user_nickname FROM user_rooms WHERE id = ?")
    (tq::bind-parameters get-rs room-id)
    (let ((ret (when (sqlite:step-statement get-rs)
                 (sqlite:statement-column-value get-rs 0))))
      (when (and ret (> (length ret) 0))
        ret))))

(defun set-room-user-nickname (room-id nickname)
  "Set the user's joined nickname for ROOM-ID to NICKNAME."
  (tq::with-prepared-statement
      (set-rn "UPDATE user_rooms SET user_nickname = ? WHERE id = ?")
    (tq::bind-parameters set-rn nickname room-id)
    (sqlite:step-statement set-rn)))

(defun insert-room-joined (room-id jid)
  "Add JID into the list of full JIDs joined to ROOM-ID."
  (tq::with-prepared-statement
      (insert-rj "INSERT INTO user_room_joined (room_id, jid) VALUES (?, ?) ON CONFLICT DO NOTHING")
    (tq::bind-parameters insert-rj room-id jid)
    (sqlite:step-statement insert-rj)))

(defun remove-room-joined (room-id jid)
  "Remove JID from the list of full JIDs joined to ROOM-ID."
  (tq::with-prepared-statement
      (delete-rj "DELETE FROM user_room_joined WHERE room_id = ? AND jid = ?")
    (tq::bind-parameters delete-rj room-id jid)
    (sqlite:step-statement delete-rj)))

(defun get-room-participants (room-id)
  "Retrieve the list of participants in ROOM-ID, as a list of (NICKNAME . MODERATOR) cons pairs."
  (tq::with-prepared-statement
      (get-members "SELECT nickname, moderator FROM user_room_participants WHERE room_id = ?")
    (tq::bind-parameters get-members room-id)
    (loop
      while (sqlite:step-statement get-members)
      collect (tq::with-bound-columns (nick moderator) get-members
                (cons nick (eql moderator 1))))))

(defun get-user-room-id (for-uid localpart)
  "Get the room ID of a room with LOCALPART for the uid FOR-UID, returning NIL if no such room exists."
  (tq::with-prepared-statement
      (get-rid "SELECT id FROM user_rooms WHERE user_id = ? AND jid = ?")
    (tq::bind-parameters get-rid for-uid localpart)
    (when (sqlite:step-statement get-rid)
      (first (tq::column-values get-rid)))))

(defun intent-get-user-room-id (for-uid localpart)
  "Get the room ID of a room with LOCALPART for the uid FOR-UID, creating the room if it does not exist."
  (tq::with-prepared-statement
      (insert-new-room "INSERT INTO user_rooms (user_id, jid, title, title_source) VALUES (?, ?, ?, ?)")
    (tq::with-transaction ()
      (alexandria:if-let ((existing-id (get-user-room-id for-uid localpart)))
        existing-id
        (let* ((stored-room-name (get-roomlist-name for-uid localpart))
               (room-name (or
                           stored-room-name
                           (format nil "~A on ~A" localpart (get-component-name))))
               (room-source (if stored-room-name 2 0)))
          (tq::bind-parameters insert-new-room for-uid localpart room-name room-source)
          (sqlite:step-statement insert-new-room)
          (format *debug-io* "~&-> Registered new room '~A' (~A) for ~A~%" room-name localpart (get-user-jid for-uid))
          (sqlite:last-insert-rowid tq::*db*))))))

(defun update-room-name-if-lower-than (room-id new-name name-source)
  "If the room source of ROOM-ID is lower than NAME-SOURCE, set the room name to NEW-NAME."
  (tq::with-prepared-statement
      (update "UPDATE user_rooms SET title = ?, title_source = ? WHERE id = ?")
    (tq::with-transaction ()
      (let ((oldsource (nth-value 1 (get-room-name room-id))))
        (when (> name-source oldsource)
          (tq::bind-parameters update new-name name-source)
          (sqlite:step-statement update))))))

(defun set-room-subject (room-id subject &optional setter)
  "Set the subject for ROOM-ID to SUBJECT."
  (tq::with-prepared-statement
      (upsert "INSERT INTO user_room_subject (room_id, subject, setter) VALUES (?, ?, ?) ON CONFLICT (room_id) DO UPDATE SET subject = excluded.subject, setter = excluded.setter")
    (let ((setter (or setter "")))
      (tq::bind-parameters upsert room-id subject setter))
    (sqlite:step-statement upsert)))

(defun clear-room-participants (room-id)
  "Delete all participants for the ROOM-ID."
  (tq::with-prepared-statement
      (clear "DELETE FROM user_room_participants WHERE room_id = ?")
    (tq::bind-parameters clear room-id)
    (sqlite:step-statement clear)))

(defun insert-room-participant (room-id nickname &optional moderatorp)
  "Insert NICKNAME into ROOM-ID's participants list."
  (tq::with-prepared-statement
      (insert "INSERT INTO user_room_participants (room_id, nickname, moderator) VALUES (?, ?, ?)")
    (let ((moderatorp (if moderatorp 1 0)))
      (tq::bind-parameters insert room-id nickname moderatorp)
      (sqlite:step-statement insert))))

(defun update-room-participant (room-id oldname newname &optional moderatorp)
  "Rename the ROOM-ID's participant OLDNAME to NEWNAME."
  (tq::with-prepared-statement
      (update "UPDATE user_room_participants SET nickname = ?, moderator = ? WHERE room_id = ? AND nickname = ?")
    (let ((moderatorp (if moderatorp 1 0)))
      (tq::bind-parameters update newname moderatorp room-id oldname)
      (sqlite:step-statement update))))

