# Joining rooms with spectrum2

- Send ipc-join-chat
- Get back a participant-changed as standard
- Get back a room-nickname-changed as standard if nickname differs
- Get back a room-participant-changed as standard if nickname differs

If the room exists:

- Get back a room-subject-changed (if it's nonzero)
- Get back a number of participants (if there are any)
- Get back another room-subject-changed
- Get back a room information

## Problem

There's no way to determine "oh this room exists, and we've joined!"

- We need a new protobuf type for this, and we'll hook it to the room being
  presented.
  - Actually we can reuse the existing "join room" message; that's good.
- Then, we register the room with a boring name (if none exists) on this hook.
