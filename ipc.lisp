(defpackage :spectrum2-ipc
  (:nicknames :s2i)
  (:use :cl))

(in-package :spectrum2-ipc)

(defclass spectrum2-ipc-message ()
  ((message-type
    :type pbnetwork:wrapper-message-type
    :reader message-type
    :initarg :message-type)
   (payload
    :reader payload
    :initform nil
    :initarg :payload)))

(defmethod print-object ((obj spectrum2-ipc-message) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (message-type payload) obj
      (format stream "~A~@[ ~A~]" message-type payload))))

(defmacro generate-ipc-message-classes (&body syms-with-numbers)
  "For each (symbol . number) cons pair in SYMS-WITH-NUMBERS, generate a class definition called IPC-[SYMBOL] that subclasses SPECTRUM2-IPC-MESSAGE. Furthermore, generate a function called IPC-MESSAGE-CLASS-FROM-NUMBER that outputs a (nullable) class object when given a number to look up in the provided list."
  (let ((class-definitions)
        (defmethods)
        (case-items)
        (wrapper-type-sym (gensym)))
    (loop
      for (number . symbol) in syms-with-numbers
      do (let* ((class-name (concatenate 'string "IPC-" (symbol-name symbol)))
                (class-sym (intern class-name :spectrum2-ipc))
                (temp (gensym)))
           (push `(defclass ,class-sym (spectrum2-ipc-message) ()) class-definitions)
           (push `(defmethod find-wrapper-type-for-message-class ((,temp ,class-sym))
                    ,number)
                 defmethods)
           (push `(,number (quote ,class-sym)) case-items)))
    `(progn
       ,@class-definitions
       (defgeneric find-wrapper-type-for-message-class (message-class)
         (:documentation "Outputs the value of the WrapperMessage Type enum for the provided MESSAGE-CLASS, an IPC message class."))
       ,@defmethods
       (defun find-message-class-for-wrapper-type (,wrapper-type-sym)
         "Find the correct IPC message class for WRAPPER-TYPE, a WrapperMessage Type enum."
         (check-type ,wrapper-type-sym pbnetwork:wrapper-message-type)
         (alexandria:when-let
             ((class-sym
               (case ,wrapper-type-sym
                 ,@case-items
                 (t nil))))
           (find-class class-sym))))))

(generate-ipc-message-classes
 (1 . connected)
 (2 . disconnected)
 (3 . login)
 (4 . logout)
 (6 . buddy-changed)
 (7 . buddy-removed)
 (8 . conv-message)
 (9 . ping)
 (10 . pong)
 (11 . join-room)
 (12 . leave-room)
 (13 . participant-changed)
 (14 . room-nickname-changed)
 (15 . room-subject-changed)
 (16 . vcard)
 (17 . status-changed)
 (18 . buddy-typing)
 (19 . buddy-stopped-typing)
 (20 . buddy-typed)
 (21 . auth-request)
 (22 . attention)
 (23 . stats)
 (29 . exit)
 (30 . backend-config)
 (31 . query)
 (32 . room-list)
 (33 . conv-message-ack)
 (34 . raw-xml)
 (35 . buddies)
 (36 . api-version)
 (37 . room-information))

(defun find-payload-class-for-wrapper-type (wrapper-type)
  "Find the correct protobuf class for WRAPPER-TYPE, a WrapperMessage Type enum."
  (check-type wrapper-type pbnetwork:wrapper-message-type)
  ;; CASE doesn't support constants, big sad. We could write a macro, but I'm also lazy, so...
  (alexandria:when-let
      ((class-sym
        (case wrapper-type
          (1 'pbnetwork:connected)
          (2 'pbnetwork:disconnected)
          (3 'pbnetwork:login)
          (4 'pbnetwork:logout)
          ;; What the heck happened to 5?!?
          (6 'pbnetwork:buddy) ; buddy changed
          (7 'pbnetwork:buddy) ; buddy removed
          (8 'pbnetwork:conversation-message) ; conv message
          ;; 9 is ping
          ;; 10 is pong
          (11 'pbnetwork:room) ; join room
          (12 'pbnetwork:room) ; leave room
          (13 'pbnetwork:participant) ; participant changed
          (14 'pbnetwork:room) ; room nickname changed
          (15 'pbnetwork:conversation-message) ; room subject changed (?)
          (16 'pbnetwork:vcard)
          (17 'pbnetwork:status) ; status changed
          (18 'pbnetwork:buddy) ; buddy typing
          (19 'pbnetwork:buddy) ; buddy stopped typing
          (20 'pbnetwork:buddy) ; buddy typed (finished typing?)
          (21 'pbnetwork:buddy) ; auth request
          (22 'pbnetwork:conversation-message) ; attention
          (23 'pbnetwork:stats)
          ;; items 24-28 are to do with file transfers, which suck
          ;; 29 is exit
          (30 'pbnetwork:backend-config) ; actual backend config
          (31 'pbnetwork:backend-config) ; admin query (WHY DOES IT USE THIS DATA TYPE?!)
          (32 'pbnetwork:room-list)
          (33 'pbnetwork:conversation-message) ; conversation message ack (?)
          ;; item 34 is raw XML, which sucks
          (35 'pbnetwork:buddies)
          (36 'pbnetwork:api-version)
          (37 'pbnetwork:room-information)
          (t nil))))
    (find-class class-sym)))

(defun serialize-pb-object (obj)
  "Serializes OBJ, a protobuf object, to an array."
  (check-type obj pb:protocol-buffer)
  (let ((ret (make-array (pb:octet-size obj)
                         :element-type '(unsigned-byte 8))))
    (pb:serialize obj ret 0 (length ret))
    ret))

(defun parse-pb-object (vector class)
  "Parse an object of class CLASS from the bytes stored in VECTOR."
  (assert (subtypep class 'pb:protocol-buffer) () "~A is not a protobuf object class." class)
  (let ((ret (make-instance class)))
    (pb:merge-from-array ret vector 0 (length vector))
    ret))

(defun parse-ipc-wrapper-message (wrapper-message)
  "Parse a WRAPPER-MESSAGE object into an IPC message class. Signals an error if the wrapper message or payload is invalid."
  (check-type wrapper-message pbnetwork:wrapper-message)
  (let* ((wrapper-type (pbnetwork:type wrapper-message))
         (ipc-message-class (or
                             (find-message-class-for-wrapper-type wrapper-type)
                             (error "Unknown message type ~A" wrapper-type)))
         (payload-class (find-payload-class-for-wrapper-type wrapper-type))
         (pb-payload (pbnetwork:payload wrapper-message))
         (payload (when (and pb-payload payload-class)
                    (parse-pb-object pb-payload payload-class))))
    (make-instance ipc-message-class
                   :message-type wrapper-type
                   :payload payload)))

(defun deserialize-ipc-buffer (buf)
  "Deserialize the IPC buffer BUF, parse the message contained therein, and return it. Signals an error if one of those steps fails."
  (check-type buf (simple-array (unsigned-byte 8)))
  (parse-ipc-wrapper-message
   (parse-pb-object buf (find-class 'pbnetwork:wrapper-message))))

(defun read-ipc-buffer (stream)
  "Read an IPC buffer from STREAM and return it as an array."
  (check-type stream stream)
  ;; The header is a network-byte-order uint32_t.
  (let* ((length (nibbles:read-ub32/be stream))
         (vector (make-array length
                             :element-type '(unsigned-byte 8))))
    (read-sequence vector stream)
    vector))

(defun serialize-ipc-message (message)
  "Serialize the IPC message object MESSAGE to an array, returning the array."
  (check-type message spectrum2-ipc-message)
  (let* ((message-type (find-wrapper-type-for-message-class message))
         (payload (payload message))
         (serialized-payload (when payload (serialize-pb-object payload)))
         (wrapper-message (make-instance 'pbnetwork:wrapper-message)))
    (setf (pbnetwork:type wrapper-message) message-type)
    (when payload
      (setf (pbnetwork:payload wrapper-message) serialized-payload))
    (serialize-pb-object wrapper-message)))

(defun write-ipc-message (stream message)
  "Writes the IPC message object MESSAGE to STREAM, serializing it first."
  (check-type stream stream)
  (check-type message spectrum2-ipc-message)
  (let ((serialized (serialize-ipc-message message)))
    (nibbles:write-ub32/be (length serialized) stream)
    (write-sequence serialized stream)
    (finish-output stream)))

(defun read-ipc-message (stream)
  "Reads an IPC message object from STREAM, returning it."
  (check-type stream stream)
  (deserialize-ipc-buffer (read-ipc-buffer stream)))
