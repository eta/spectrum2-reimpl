(in-package :spectrum2-ipc)

(defun make-ipc-login (for-user username password)
  "Makes an IPC-LOGIN message."
  (let ((payload (make-instance 'pbnetwork:login)))
    (setf (pbnetwork:legacy-name payload) (pb:string-field username))
    (setf (pbnetwork:user payload) (pb:string-field for-user))
    (setf (pbnetwork:password payload) (pb:string-field password))
    (make-instance 'ipc-login :payload payload)))

(defun make-conv-message (for-user buddy-name message &key id xhtml)
  "Makes an IPC-CONV-MESSAGE message."
  (let ((payload (make-instance 'pbnetwork:conversation-message)))
    (setf (pbnetwork:user-name payload) (pb:string-field for-user))
    (setf (pbnetwork:buddy-name payload) (pb:string-field buddy-name))
    (setf (pbnetwork:message payload) (pb:string-field message))
    (when id
      (setf (pbnetwork:id payload) (pb:string-field id)))
    (when xhtml
      (setf (pbnetwork:xhtml payload) (pb:string-field xhtml)))
    (make-instance 'ipc-conv-message :payload payload)))

(defun make-join-room (for-user nickname room-name &optional password)
  "Makes an IPC-JOIN-ROOM message."
  (let ((payload (make-instance 'pbnetwork:room)))
    (setf (pbnetwork:user-name payload) (pb:string-field for-user))
    (setf (pbnetwork:nickname payload) (pb:string-field nickname))
    (setf (pbnetwork:room payload) (pb:string-field room-name))
    (when password
      (setf (pbnetwork:password payload) password))
    (make-instance 'ipc-join-room :payload payload)))
